﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Servicos;
using System.Security.Claims;

namespace Apresentacao.Controllers
{
    [Route("api/[Controller]")]
    [ApiController]
    public class AccountController:ControllerBase
    {
        private IServAccount _servAccount;
        public AccountController(IServAccount servAccount)
        {
            _servAccount = servAccount;
        }

        [Route("accountinfo")]
        [HttpGet]
        [Authorize]
        public ActionResult AccountInfo()
        {
            try
            {
                int userId = Int32.Parse(User.Claims.FirstOrDefault(c => c.Type == "UserId").Value);

                var accountInfo = _servAccount.GetAccountDetails(userId);

                return Ok(accountInfo);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
