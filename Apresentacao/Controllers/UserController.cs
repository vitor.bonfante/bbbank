﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Servicos;
using Servicos.DTO;

namespace Apresentacao
{
    [Route("api/[Controller]")]
    [ApiController]
    public class UserController: ControllerBase
    {
        private IServUser _servUser;
        public UserController(IServUser servUser)
        {
            _servUser = servUser;
        }
        [Route("register")]
        [HttpPost]
        [AllowAnonymous]
        public ActionResult Register(CreateNewUserDTO createNewUserDTO)
        {
            try
            {
                _servUser.CreateNewUser(createNewUserDTO);

                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
        [Route("login")]
        [HttpPost]
        [AllowAnonymous]
        public ActionResult Login([FromBody]LoginDTO loginDTO)
        {
            try
            {
                var result = _servUser.Login(loginDTO);

                return Ok(result);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
    