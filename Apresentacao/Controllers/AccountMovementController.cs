﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Servicos;
using Servicos.DTO;

namespace Apresentacao.Controllers
{
    [Route("api/[Controller]")]
    [ApiController]
    public class AccountMovementController:ControllerBase
    {
        private IServAccountMovements _servAccountMovements;
        public AccountMovementController(IServAccountMovements servAccountMovements)
        {
            _servAccountMovements = servAccountMovements;
        }

        [Route("withdraw")]
        [HttpPost]
        [Authorize]
        public ActionResult Withdraw([FromBody]ValueDTO valueDTO)
        {
            try
            {
                string AccountNumber = User.Claims.FirstOrDefault(c => c.Type == "AccountNumber").Value;

                CreateNewMovementFromDTO createNewMovementFromDTO = new CreateNewMovementFromDTO()
                {
                    FromAccountNumber = AccountNumber,
                    Value = valueDTO.Value
                };

                _servAccountMovements.CreateNewWithdrawMovement(createNewMovementFromDTO);

                return Ok("Saque realizado com sucesso");
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [Route("deposit")]
        [HttpPost]
        [Authorize]
        public ActionResult Deposit([FromBody] ValueDTO valueDTO)
        {
            try
            {
                string AccountNumber = User.Claims.FirstOrDefault(c => c.Type == "AccountNumber").Value;

                CreateNewMovementToDTO createNewMovementToDTO = new CreateNewMovementToDTO()
                {
                    ToAccountNumber = AccountNumber,
                    Value = valueDTO.Value
                };

                _servAccountMovements.CreateNewDepositMovement(createNewMovementToDTO);

                return Ok("Deposito realizado com sucesso");
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [Route("transfer")]
        [HttpPost]
        [Authorize]
        public ActionResult Transfer([FromBody] TransferDTO transferDTO)
        {
            try
            {
                string FromAccountNumber = User.Claims.FirstOrDefault(c => c.Type == "AccountNumber").Value;

                CreateNewMovementFromToDTO createNewMovementFromToDTO = new CreateNewMovementFromToDTO()
                {
                    FromAccountNumber = FromAccountNumber,
                    ToAccountNumber = transferDTO.Account,
                    Value = transferDTO.Value
                };

                _servAccountMovements.CreateNewTransferMovement(createNewMovementFromToDTO);

                return Ok("Transferencia realizado com sucesso");
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [Route("payment")]
        [HttpPost]
        [Authorize]
        public ActionResult Payment([FromBody] ValueDTO valueDTO)
        {
            try
            {
                string AccountNumber = User.Claims.FirstOrDefault(c => c.Type == "AccountNumber").Value;

                CreateNewMovementFromDTO createNewMovementFromDTO = new CreateNewMovementFromDTO()
                {
                    FromAccountNumber = AccountNumber,
                    Value = valueDTO.Value
                };

                _servAccountMovements.CreateNewPaymentMovement(createNewMovementFromDTO);

                return Ok("Pagamento realizado com sucesso");
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

    }
}
