﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class AccountMovements
    {
        public int Id { get; set; }
        public int? FromAccountId { get; set; }
        public int? ToAccountId { get; set; }
        public decimal Value { get; set; }
        public DateTime MovementDate { get; set; }
        public EnumMovementType MovementType { get; set; }
        public Account FromAccount { get; set; }
        public Account ToAccount { get; set; }
    }

    public enum EnumMovementType
    {
        withdraw = 0,
        deposit = 1,
        payment = 2,
        transfer = 3,
    }
}
