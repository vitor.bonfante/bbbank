# Projeto BB Bank

## Nomes: Mateus Bratti e Vitor Bonfante

---

## 1. Prop�sito do Sistema

O prop�sito deste sistema � oferecer um software banc�rio onde os usu�rios podem abrir uma conta e realizar opera��es como saque, dep�sito, pagamentos (Boleto) e transfer�ncias entre contas.

---

## 2. Usu�rios do Sistema

Os usu�rios do sistema s�o pessoas que desejam abrir uma conta no BB Bank.

---

## 3. Requisitos Funcionais

### RF001 - Abertura de Conta

**Caso de Uso:** Abertura de Conta  
**Descri��o:** O sistema deve permitir que novos clientes abram uma conta banc�ria, fornecendo informa��es pessoais. Ap�s a valida��o das informa��es, o sistema deve gerar um n�mero de conta �nico.

### RF002 - Opera��es de Saque

**Caso de Uso:** Opera��es de Saque  
**Descri��o:** Os clientes devem poder realizar saques de suas contas banc�rias. Antes de permitir o saque, o sistema deve verificar se h� saldo dispon�vel e atualizar o saldo da conta ap�s a conclus�o do saque.

### RF003 - Opera��es de Dep�sito

**Caso de Uso:** Opera��es de Dep�sito  
**Descri��o:** Os clientes devem poder fazer dep�sitos em suas contas banc�rias. O sistema deve verificar a autenticidade do valor depositado e atualizar o saldo da conta ap�s a conclus�o do dep�sito.

### RF004 - Pagamentos de Boleto

**Caso de Uso:** Pagamentos de Boleto  
**Descri��o:** Os clientes devem poder pagar boletos banc�rios atrav�s do sistema. O sistema deve permitir a entrada manual dos dados do boleto ou a leitura autom�tica do c�digo de barras, validar as informa��es do boleto e atualizar o saldo da conta ap�s o pagamento.

### RF005 - Transfer�ncias entre Contas

**Caso de Uso:** Transfer�ncias entre Contas  
**Descri��o:** Os clientes devem poder transferir fundos entre contas banc�rias. O sistema deve solicitar o n�mero da conta de destino e o valor a ser transferido, verificar a disponibilidade de fundos na conta de origem e atualizar o saldo das contas de origem e destino ap�s a transfer�ncia.

### RF006 - Seguran�a e Autentica��o

**Caso de Uso:** Seguran�a e Autentica��o  
**Descri��o:** O sistema deve garantir a seguran�a das informa��es dos clientes. Deve exigir autentica��o do usu�rio atrav�s de um Token para acessar as informa��es da conta.

