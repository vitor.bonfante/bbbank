﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Servicos.DTO
{
    public class UpdateAccountBalanceDTO
    {
        public int AccountId { get; set; }
        public decimal NewValue { get; set; }
    }
}
