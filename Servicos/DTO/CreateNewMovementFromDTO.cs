﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Servicos.DTO
{
    public class CreateNewMovementFromDTO
    {
        public string FromAccountNumber { get; set; }
        public decimal Value { get; set; }
    }
}
