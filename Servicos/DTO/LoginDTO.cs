﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Servicos.DTO
{
    public class LoginDTO
    {
        public string Cpf { get; set; }
        public string Password { get; set; }
    }
}
