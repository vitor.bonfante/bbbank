﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Servicos.DTO
{
    public class TransferDTO
    {
        public string Account { get; set; }
        public decimal Value { get; set; }

    }
}
