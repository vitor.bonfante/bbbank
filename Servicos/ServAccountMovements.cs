﻿using Entidades;
using Repositorio;
using Servicos.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Servicos
{
    public interface IServAccountMovements
    {
        void CreateNewWithdrawMovement(CreateNewMovementFromDTO createNewMovementFromDTO);
        void CreateNewDepositMovement(CreateNewMovementToDTO createNewMovementToDTO);
        void CreateNewPaymentMovement(CreateNewMovementFromDTO createNewMovementFromDTO);
        void CreateNewTransferMovement(CreateNewMovementFromToDTO CreateNewMovementFromToDTO);
    }
    public class ServAccountMovements: IServAccountMovements
    {
        private IRepoAccountMovements _repoAccountMovements;
        private IServAccount _servAccount;
        public ServAccountMovements(IRepoAccountMovements repoAccountMovements, IServAccount servAccount)
        {
            _repoAccountMovements = repoAccountMovements;
            _servAccount = servAccount;
        }

        public void CreateNewWithdrawMovement(CreateNewMovementFromDTO createNewMovementFromDTO)
        {
            if (createNewMovementFromDTO.Value <= 0)
            {
                throw new Exception("Valor invalido");
            }

            var checkAccount = _servAccount.GetAccountByAccountNumber(createNewMovementFromDTO.FromAccountNumber);
            if(checkAccount == null)
            {
                throw new Exception("Conta não encontrada");
            }
           
            if (checkAccount.Balance < createNewMovementFromDTO.Value)
            {
                throw new Exception("Saldo insuficiente");
            }

            var newBalance = checkAccount.Balance - createNewMovementFromDTO.Value;
            _servAccount.UpdateAccountBalance(checkAccount.Id,newBalance);

            _repoAccountMovements.Create(new AccountMovements()
            {
                FromAccount = checkAccount,
                Value = createNewMovementFromDTO.Value,
                MovementType = EnumMovementType.withdraw,
                MovementDate = DateTime.Now
            });
        }

        public void CreateNewDepositMovement(CreateNewMovementToDTO createNewMovementToDTO) 
        {
            if (createNewMovementToDTO.Value <= 0)
            {
                throw new Exception("Valor invalido");
            }

            var checkAccount = _servAccount.GetAccountByAccountNumber(createNewMovementToDTO.ToAccountNumber);
            if (checkAccount == null)
            {
                throw new Exception("Conta não encontrada");
            }

            var newBalance = checkAccount.Balance + createNewMovementToDTO.Value;
            _servAccount.UpdateAccountBalance(checkAccount.Id, newBalance);

            var movement = new AccountMovements();

            movement.ToAccount = checkAccount;
            movement.Value = createNewMovementToDTO.Value;
            movement.MovementType = EnumMovementType.deposit;
            movement.MovementDate = DateTime.Now;

            _repoAccountMovements.Create(movement);
        }
        public void CreateNewPaymentMovement(CreateNewMovementFromDTO createNewMovementFromDTO) 
        {
            if (createNewMovementFromDTO.Value <= 0)
            {
                throw new Exception("Valor invalido");
            }

            var checkAccount = _servAccount.GetAccountByAccountNumber(createNewMovementFromDTO.FromAccountNumber);
            if (checkAccount == null)
            {
                throw new Exception("Conta não encontrada");
            }

            if (checkAccount.Balance < createNewMovementFromDTO.Value)
            {
                throw new Exception("Saldo insuficiente");
            }

            var newBalance = checkAccount.Balance - createNewMovementFromDTO.Value;
            _servAccount.UpdateAccountBalance(checkAccount.Id, newBalance);

            var movement = new AccountMovements();

            movement.FromAccount = checkAccount;
            movement.Value = createNewMovementFromDTO.Value;
            movement.MovementType = EnumMovementType.payment;
            movement.MovementDate = DateTime.Now;

            _repoAccountMovements.Create(movement);
        }
        public void CreateNewTransferMovement(CreateNewMovementFromToDTO createNewMovementFromToDTO) 
        {
            if (createNewMovementFromToDTO.Value <= 0)
            {
                throw new Exception("Valor invalido");
            }

            var checkAccountFrom = _servAccount.GetAccountByAccountNumber(createNewMovementFromToDTO.FromAccountNumber);
            if (checkAccountFrom == null)
            {
                throw new Exception("Conta de origem não encontrada");
            }

            var checkAccountTo = _servAccount.GetAccountByAccountNumber(createNewMovementFromToDTO.ToAccountNumber);
            if (checkAccountTo == null)
            {
                throw new Exception("Conta de destino não encontrada");
            }

            if (checkAccountFrom.AccountNumber == checkAccountTo.AccountNumber)
            {
                throw new Exception("Conta de destino deve ser diferente da conta de origem");
            }

            if (checkAccountFrom.Balance < createNewMovementFromToDTO.Value)
            {
                throw new Exception("Saldo insuficiente");
            }

            var newBalanceFrom = checkAccountFrom.Balance - createNewMovementFromToDTO.Value;
            var newBalanceTo = checkAccountTo.Balance + createNewMovementFromToDTO.Value;
            
            _servAccount.UpdateAccountBalance(checkAccountFrom.Id, newBalanceFrom);
            _servAccount.UpdateAccountBalance(checkAccountTo.Id, newBalanceTo);

            var movement = new AccountMovements();

            movement.FromAccount = checkAccountFrom;
            movement.ToAccount = checkAccountTo;
            movement.Value = createNewMovementFromToDTO.Value;
            movement.MovementType = EnumMovementType.transfer;
            movement.MovementDate = DateTime.Now;

            _repoAccountMovements.Create(movement);
        }
    }
}
