﻿using Entidades;
using Repositorio;
using Servicos.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Servicos
{
    public interface IServAccount
    {
        void CreateNewAccount(int UserId);
        Account GetAccountDetails(int UserId);
        Account GetAccountByAccountNumber(string AccountNumber);
        void UpdateAccountBalance(int AccountId, decimal NewBalance);
    }
    public class ServAccount: IServAccount
    {
        private IRepoAccount _repoAccount;
        public ServAccount(IRepoAccount repoAccount)
        {
            _repoAccount = repoAccount;
        }

        public void CreateNewAccount(int UserId)
        {
            var account = new Account();

            account.UserId = UserId;
            account.AccountNumber = GenerateAccountNumber();
            account.Balance = 0;

            _repoAccount.Create(account);
            
        }
        private string GenerateAccountNumber() {
            var rand = GenerateRandomString();

            var check = _repoAccount.FindByAccountNumber(rand);
            if(check != null) {
                GenerateAccountNumber();
            }

            return rand;
        }
        private string GenerateRandomString()
        {
            string charsAllowed = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            Random random = new Random();
            char[] chars = new char[6];

            for (int i = 0; i < 6; i++)
            {
                chars[i] = charsAllowed[random.Next(charsAllowed.Length)];
            }

            return new string(chars);
        }

        public Account GetAccountDetails(int UserId)
        {
            var account = _repoAccount.FindByUserId(UserId);

            return account;
        }

        public Account GetAccountByAccountNumber(string AccountNumber)
        {
            var account = _repoAccount.FindByAccountNumber(AccountNumber);

            return account;
        }

        public void UpdateAccountBalance(int AccountId, decimal NewBalance)
        {
            var account = _repoAccount.FindByAccountId(AccountId);
            if(account == null)
            {
                throw new Exception("Conta não encontrada");
            }

            account.Balance = NewBalance;
            _repoAccount.Update(account);
        }

    }
}
