﻿using Entidades;
using Repositorio;
using Servicos.DTO;
using BCrypt.Net;
using static BCrypt.Net.BCrypt;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Servicos
{
    public interface IServUser
    {
        void CreateNewUser(CreateNewUserDTO createNewUserDTO);
        string Login(LoginDTO loginDTO);
    }
    public class ServUser: IServUser
    {
        private IRepoUser _repoUser;
        private IServAccount _servAccount;
        private IServToken _servToken;

        public ServUser(IRepoUser repoUser, IServAccount servAccount, IServToken servToken)
        {
            _repoUser = repoUser;
            _servAccount = servAccount;
            _servToken = servToken;
        }

        public void CreateNewUser(CreateNewUserDTO createNewUserDTO)
        {
            var user = _repoUser.FindByCpf(createNewUserDTO.Cpf);
            if (user != null)
            {
                throw new Exception("CPF já cadastrado");
            }

            var newUser = new User();

            newUser.Name = createNewUserDTO.Name;
            newUser.Cpf = createNewUserDTO.Cpf;
            newUser.Password = HashPassword(createNewUserDTO.Password, 12);

            var newUserId = _repoUser.Create(newUser);

            _servAccount.CreateNewAccount(newUserId);
        }

        public string Login(LoginDTO loginDTO)
        {
            var user = _repoUser.FindByCpf(loginDTO.Cpf);
            if (user == null)
            {
                throw new Exception("CPF ou senha inválidos");
            }

            if (!Verify(loginDTO.Password, user.Password)) 
            {
                throw new Exception("CPF ou senha inválidos");
            }

            var account = _servAccount.GetAccountDetails(user.Id);
            if (account == null)
            {
                throw new Exception("CPF ou senha inválidos");
            }

            var token = _servToken.GenerateToken(user.Id, account.AccountNumber);
            return token;
        }

    }
}
