﻿using Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repositorio
{
    public interface IRepoUser
    {
        int Create(User user);
        User FindByCpf(string cpf);
    }
    public class RepoUser: IRepoUser
    {
        private DataContext _dataContext;

        public RepoUser(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public int Create(User user)
        {
            _dataContext.Add(user);

            _dataContext.SaveChanges();

            return user.Id;
        }

        public User FindByCpf(string cpf) {
            var user = _dataContext.User.Where(p => p.Cpf == cpf).FirstOrDefault();

            return user;
        }

    }
}
