﻿using Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace Repositorio
{
    public interface IRepoAccountMovements
    {
        void Create(AccountMovements accountMovements);
    }
    public class RepoAccountMovements: IRepoAccountMovements
    {
        private DataContext _dataContext;
        public RepoAccountMovements(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public void Create(AccountMovements accountMovements)
        {
            _dataContext.Add(accountMovements);

            _dataContext.SaveChanges();
        }
    }
}
