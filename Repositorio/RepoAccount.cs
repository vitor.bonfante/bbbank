﻿using Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repositorio
{
    public interface IRepoAccount
    {
        void Create(Account account);
        Account FindByAccountNumber(string accountNumber);
        Account FindByUserId(int userId);
        Account FindByAccountId(int accountId);
        void Update(Account account);
    }
    public class RepoAccount: IRepoAccount
    {
        private DataContext _dataContext;

        public RepoAccount(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public void Create(Account account)
        {
            _dataContext.Add(account);

            _dataContext.SaveChanges();
        }

        public Account FindByUserId(int UserId)
        {
            var account = _dataContext.Account
                .Where(p => p.UserId == UserId).FirstOrDefault();

            return account;
        }
        public Account FindByAccountNumber(string accountNumber)
        {
            var account = _dataContext.Account
                .Where(p => p.AccountNumber == accountNumber).FirstOrDefault();

            return account;
        }

        public void Update(Account account)
        {
            _dataContext.SaveChanges();
        }

        public Account FindByAccountId(int accountId)
        {
            var account = _dataContext.Account
                .Where(p => p.Id == accountId).FirstOrDefault();

            return account;
        }
    }
}
