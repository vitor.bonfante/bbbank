﻿using Entidades;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repositorio
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {

        }

        public DbSet<User> User { get; set; }
        public DbSet<Account> Account { get; set; }
        public DbSet<AccountMovements> AccountMovements { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().HasKey(p => p.Id);
            modelBuilder.Entity<Account>().HasKey(p => p.Id);
            modelBuilder.Entity<AccountMovements>().HasKey(p => p.Id);

            modelBuilder.Entity<User>()
                .HasOne(p => p.Account)
                .WithOne(p => p.User)
                .HasForeignKey<Account>(p => p.UserId)
                .IsRequired();
            modelBuilder.Entity<AccountMovements>()
                .HasOne(p => p.FromAccount)
                .WithMany()
                .HasForeignKey(p => p.FromAccountId);
            modelBuilder.Entity<AccountMovements>()
                .HasOne(p => p.ToAccount)
                .WithMany()
                .HasForeignKey(p => p.ToAccountId);

            base.OnModelCreating(modelBuilder);
        }
    }
}
